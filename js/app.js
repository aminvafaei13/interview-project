/**
 * Created by Mohammad Amin on 7/18/2017.
 */


var app = angular.module('myApp', ['ui.bootstrap']);
app.controller('myCtrl', function ($rootScope,$scope, $uibModal) {
    $scope.reserve = function (item) {

        var modalInstance = $uibModal.open({
            templateUrl: 'ReserveModal.html',
            controller: 'ReserveCtrl',
            size:'lg',
            resolve: {
                item: function () {
                    return item;
                }
            }
        });
        modalInstance.result.then(function (selectedItem) {
        }, function () {
        });
    };
    $rootScope.getNumber = function (num) {
        return new Array(num);
    }
    $scope.travels = [{
        name: 'Test1',
        capital: 'Capital of France',
        des: 'test test test test test test test test test test test test ',
        price: 500,
        star: 3,
        day: 6,
        night: 5,
        img: 'img/portfolio/fullsize/1.jpg'
    }, {
        name: 'Test2',
        capital: 'Capital of France',
        des: 'test test test test test test test test test test test test ',
        price: 700,
        star: 5,
        day: 8,
        night: 7,
        img: 'img/portfolio/fullsize/2.jpg'
    }, {
        name: 'Test3',
        capital: 'Capital of France',
        des: 'test test test test test test test test test test test test ',
        price: 1050,
        star: 7,
        day: 3,
        night: 2,
        img: 'img/portfolio/fullsize/3.jpg'
    }, {
        name: 'Test4',
        capital: 'Capital of France',
        des: 'test test test test test test test test test test test test ',
        price: 5500,
        star: 2,
        day: 7,
        night: 6,
        img: 'img/portfolio/fullsize/4.jpg'
    }, {
        name: 'Test5',
        capital: 'Capital of France',
        des: 'test test test test test test test test test test test test ',
        price: 2000,
        star: 6,
        day: 4,
        night: 3,
        img: 'img/portfolio/fullsize/5.jpg'
    }, {
        name: 'Test6',
        capital: 'Capital of France',
        des: 'test test test test test test test test test test test test ',
        price: 1500,
        star: 4,
        day: 5,
        night: 4,
        img: 'img/portfolio/fullsize/6.jpg'
    }]
});
app.controller('ReserveCtrl', function ($scope, $uibModal, item,$uibModalInstance) {
    $scope.item = item;
    console.log($scope.item);
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
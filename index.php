<div ng-app="myApp" ng-controller="myCtrl">
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="web site for interview">
        <meta name="author" content="Mohammad Amin">
        <title>تست مصاحبه</title>
        <!-- Bootstrap Core CSS -->
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="vendor/muicss/css/mui-rtl.css" rel="stylesheet">
        <link href="vendor/font/master.css" rel="stylesheet">
        <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- Plugin CSS -->
        <link href="vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
        <!-- Theme CSS -->
        <link href="css/creative.min.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body id="page-top">
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">International Tours</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#services">Tours</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <header>
        <div class="header-content">
            <div class="header-content-inner col-md-8">
                <h1 id="homeHeading">International Tours</h1>
                <hr>
                <p class="sub-title">this is test message or info!!this is test message or info!!this is test message or info!!this is test message or info!!this is test message or info!!</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
            </div>
            <div class="header-content-inner col-md-4">
                <div class="row login-back">
                    <div class="col-md-12">
                        <span style="color: #fff">LOGIN VIA</span>
                        <div class="social-buttons">
                            <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                            <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                        </div>
                        <span style="color: #fff">OR</span>
                        <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="exampleInputPassword2">Password</label>
                                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                <div class="help-block text-right"><a href="">Forget the password ?</a></div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                            </div>
                        </form>
                    </div>
                    <div class="bottom text-center">
                        <a href="#"><b>Join Us</b></a>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-4 " ng-repeat="x in travels">
                    <div class="card">
                        <div class="card-image">
                        <span class="card-title-price">
                            <span class="pull-right price">{{x.price | currency }}</span>
                            <span class="star">

                                    <i ng-repeat="i in getNumber(x.star) track by $index" class="glyphicon glyphicon-star gold"></i>



                                </span>
                        </span>
                            <img class="img-responsive" ng-src={{x.img}}>
                            <span class="card-title">

                                <span class="pull-right days"><i class="fa fa-sun-o"></i> {{x.day}}
                                - {{x.night}}  <i class="fa fa-moon-o"></i></span>
                                <span>{{x.name}}</span>
                            </span>
                        </div>
                        <div class="card-content">
                            <p>{{x.des}}</p>
                        </div>
                        <div class="card-action">
                            <a ng-click="reserve(x)" style="cursor: pointer;">Reserve</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>It's a test Message</h2>
                <a class="btn btn-default btn-xl sr-button">Download Our App</a>
            </div>
        </div>
    </aside>
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">About US</h2>
                    <hr class="primary">
                    <p>test about us test about us test about us test about us test about us test about us </p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>123-456-6789</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:your-email@your-domain.com">mail@mail.com</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/angular/angular.min.js"></script>
    <script src="vendor/angular/ui-bootstrap-tpls-2.5.0.min.js"></script>
    <script src="vendor/muicss/js/mui.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="vendor/jquery/jquery.easing.min.js"></script>
    <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Theme JavaScript -->
    <script src="js/creative.min.js"></script>
    <script src="js/app.js"></script>
    </body>
    </html>
    <script type="text/ng-template" id="ReserveModal.html">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">{{item.name}}</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <div class="row">
                <div class="col-md-6 ">
                    <div class="card">
                        <div class="card-image">
                        <span class="card-title-price">
                            <span class="pull-right price">{{item.price | currency }}</span>
                            <span class="star">

                                    <i ng-repeat="i in $root.getNumber(item.star) track by $index" class="glyphicon glyphicon-star gold"></i>



                                </span>
                        </span>
                            <img class="img-responsive" ng-src={{item.img}} style="position:static !important;">
                            <span class="card-title">

                                <span class="pull-right days"><i class="fa fa-sun-o"></i> {{item.day}}
                                - {{item.night}}  <i class="fa fa-moon-o"></i></span>
                                <span>{{item.name}}</span>
                            </span>
                        </div>
                        <div class="card-content">
                            <p>{{item.des}}</p>
                        </div>

                    </div>
                </div>

                <div class="col-md-6">


                    <form class="mui-form">

                        <div class="mui-textfield">
                            <input type="text">
                            <label>Name</label>
                        </div>
                        <div class="mui-textfield">
                            <input type="text">
                            <label>Last Name</label>
                        </div>
                        <div class="mui-textfield">
                            <input type="text">
                            <label>Email</label>
                        </div>
                        <div class="mui-textfield">
                            <input type="text">
                            <label>Number</label>
                        </div>
                        <button type="submit" class="mui-btn mui-btn--raised">Submit</button>
                        <button   class="mui-btn mui-btn--raised mui-btn--danger" ng-click="cancel()">Cancel</button>
                    </form>

                </div>
            </div>
        </div>
        <div class="modal-footer">


        </div>
    </script>
</div>